<?php
/**
 * Functions
 *
 * @package Append Custom Field to Grade Level plugin
 */

add_action( 'functions/GetGrade.fnc.php|filter_grade_level', 'AppendCustomFieldToGradeLevel', 3 );


function AppendCustomFieldToGradeLevel( $tag, &$return_value, $column )
{
	global $THIS_RET;

	if ( $column !== 'TITLE'
		|| empty( $THIS_RET['STUDENT_ID'] ) )
	{
		return false;
	}

	if ( Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_LIST_ONLY' )
		&& ( empty( $_REQUEST['search_modfunc'] )
			|| $_REQUEST['search_modfunc'] !== 'list' ) )
	{
		return false;
	}

	$column = Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN' );

	if ( ! $column )
	{
		return false;
	}

	if ( array_key_exists( $column, $THIS_RET ) )
	{
		$value = $THIS_RET[ $column ];
	}
	elseif ( $column === 'HOSTEL_ROOM' )
	{
		$value = DBGetOne( "SELECT hr.TITLE
			FROM hostel_rooms hr,hostel_students hs
			WHERE hs.STUDENT_ID='" . (int) $THIS_RET['STUDENT_ID'] . "'
			AND hr.ID=hs.ROOM_ID
			LIMIT 1" );
	}
	else
	{
		$value = DBGetOne( "SELECT " . DBEscapeIdentifier( $column ) . "
			FROM students
			WHERE STUDENT_ID='" . (int) $THIS_RET['STUDENT_ID'] . "'" );
	}

	$sep = Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SEPARATOR' );

	$return_value .= $sep . $value;

	// Sort Order.
	$sort_order = Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SORT_ORDER' );

	if ( $sort_order === 'COLUMN' )
	{
		// Remove Grade Level Sort Order in HTML comment.
		// Add column in HTML comment.
		$return_value = '<!-- ' . $value . ' -->' . strip_tags( $return_value );
	}

	// Second custom field.
	$column2 = Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2' );

	if ( ! $column2
		|| $column2 === $column )
	{
		return true;
	}

	if ( array_key_exists( $column2, $THIS_RET ) )
	{
		$value2 = $THIS_RET[ $column2 ];
	}
	elseif ( $column2 === 'HOSTEL_ROOM' )
	{
		$value2 = DBGetOne( "SELECT hr.TITLE
			FROM hostel_rooms hr,hostel_students hs
			WHERE hs.STUDENT_ID='" . (int) $THIS_RET['STUDENT_ID'] . "'
			AND hr.ID=hs.ROOM_ID
			LIMIT 1" );
	}
	else
	{
		$value2 = DBGetOne( "SELECT " . DBEscapeIdentifier( $column2 ) . "
			FROM students
			WHERE STUDENT_ID='" . (int) $THIS_RET['STUDENT_ID'] . "'" );
	}

	$return_value .= $sep . $value2;

	if ( $sort_order === 'COLUMN_2' )
	{
		// Remove Grade Level Sort Order in HTML comment.
		// Add column 2 in HTML comment.
		$return_value = '<!-- ' . $value2 . ' -->' . strip_tags( $return_value );
	}

	return true;
}
