/**
 * Install PostgreSQL
 * - Add program config options if any (to every schools)
 *
 * @package Append Custom Field to Grade Level plugin
 */

/**
 * config Table
 *
 * syear: school year (school may have various years in DB)
 * school_id: may exists various schools in DB
 * program: convention is plugin name, for ex.: 'email_smtp'
 * title: for ex.: 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_[your_config]'
 * value: string
 */
--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: rosariosis
--

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_LIST_ONLY', 'Y'
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_LIST_ONLY');

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN');

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2');

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SEPARATOR', ' / '
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SEPARATOR');

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SORT_ORDER', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SORT_ORDER');
