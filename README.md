# Append Custom Field to Grade Level plugin

![screenshot](https://gitlab.com/francoisjacquet/Append_Custom_Field_to_Grade_Level/raw/main/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/Append_Custom_Field_to_Grade_Level/

Version 3.0 - February, 2024

Author François Jacquet

Sponsored by AT group, Slovenia

License GNU/GPLv2 or later

## Description

This plugin lets you append up to 2 Custom Student Fields to the Grade Level. This is useful to include other important information about students (like their group) on listings without the need to click on "Expanded View".

Possible student fields include the Username, [Hostel](https://www.rosariosis.org/modules/hostel/) Room, and fields of the Text, Pull-Down, and Auto Pull-Down types.

You can configure the separator (up to 3 characters added between the grade level and the field value).

You can also choose whether the custom field is appended when displaying Student Listing only, or everywhere (for example on _Report Cards_ too).

Finally, you can choose whether to sort the column by Grade Level (default), the first chosen Student Field or the second one. Sorting is actually done when clicking on the "Grade Level" link in the list header.

Translated in French & Spanish.

Note: on the screenshot, the Username is appended to the Grade Level, and the separator is " / ".

## Content

Plugin Configuration

- Student Field
- Separator
- Student Listing only
- Sort Column by

## Install

Copy the `Append_Custom_Field_to_Grade_Level/` folder (if named `Append_Custom_Field_to_Grade_Level-main`, rename it) and its content inside the `plugins/` folder of RosarioSIS.

Go to _School > Configuration > Plugins_ and click "Activate".

Requires RosarioSIS 11.4+
